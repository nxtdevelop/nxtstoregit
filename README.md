# nxtstoregit

## Comandos para configurar servidor ##

Backend:

python3 -m venv env
source env/bin/source

pip install -r requirements.txt

python3 manage.py createsuperuser

python3 manage.py runserver

Frontend:

cd nxtstoregit
source env/bin/source
cd frontend

npm i
npm run dev
