import React from "react";
import AdCarousel from "../components/AdCarrousel";
import { useNavigate } from "react-router-dom";

const ServicesPage: React.FC = () => {
const navigate = useNavigate();
const adImages = ["./carousel-cast.png", "./carousel-pc.png", "./carousel-partner.png"];
const redirectToStore = () => {
    // Redirige a la página de la tienda (/store) al hacer clic en el botón o las imágenes
    navigate("/");
};
  const services = [
    {
      title: "Casting de Eventos",
      description: "Disfruta de un casteo de tus eventos completamente profesional y divertido. Ya sea un partido de fútbol, un torneo de algún videojuego, ¡te tenemos cubierto!",
      contactLink: "https://www.facebook.com/profile.php?id=61550964752785"
    },
    {
      title: "Armado de PCs",
      description: "¿Te da un poco de miedo armar tu propia PC? Nosotros te ayudamos. Ya sea una cotización o un armado con tus propias piezas. Nosotros lo hacemos y con gusto te enseñamos en el proceso.",
      contactLink: "https://www.facebook.com/profile.php?id=61550964752785"
    },
    {
      title: "Patrocinio",
      description: "Si crees que tu equipo da el ancho en torneos y partidos, nosotros podemos ayudarte a que llegue aún más lejos con un cariñoso patrocinio de nuestra parte que puede negociarse para adaptarse a sus necesidades.",
      contactLink: "https://www.facebook.com/profile.php?id=61550964752785"
    }
  ];

  return (
    <div className="bg-white dark:bg-gray-800">
      <div className="max-w-screen-xl mx-auto flex p-8 text-gray-900 dark:text-white">
        {/* Collage de imágenes circulares y textos */}
        <div className="flex flex-col items-center space-y-4 mr-8">
          <div className="w-16 h-16 rounded-full overflow-hidden border-2 border-white">
            <img src="./casteo.png" alt="Service Image 1" className="w-full h-full object-cover" />
          </div>
          <h1 className="text-xl font-bold">Casting de Eventos</h1>
        </div>
        <div className="flex flex-col items-center space-y-4 mr-8">
          <div className="w-16 h-16 rounded-full overflow-hidden border-2 border-white">
            <img src="./PC.png" alt="Service Image 2" className="w-full h-full object-cover" />
          </div>
          <h1 className="text-xl font-bold">Armado de PCs</h1>
        </div>
        <div className="flex flex-col items-center space-y-4">
          <div className="w-16 h-16 rounded-full overflow-hidden border-2 border-white">
            <img src="./patrocinio.png" alt="Service Image 3" className="w-full h-full object-cover" />
          </div>
          <h1 className="text-xl font-bold">Patrocinio</h1>
        </div>
        {/* Carrusel de imágenes */}
        <div className="flex flex-col w-1/2 ml-8">
            <div id="carousel" className="w-1/2 mx-auto mt-0 px-8">
                <AdCarousel adImages={adImages} />
            </div>
            <br />
            <button
            className="mt-4 bg-gray-900 text-white px-6 py-3 rounded-full hover:bg-blue-700"
            onClick={redirectToStore}
            >
            Regresar al inicio
            </button>
        </div>
      </div>
      {/* Detalles de servicios */}
      <div className="max-w-screen-xl mx-auto p-8 text-gray-900 dark:text-white">
        {services.map((service, index) => (
          <div key={index} className="mb-8">
            <h1 className="text-2xl font-bold mb-2">{service.title}</h1>
            <p>{service.description}</p>
            <p className="mt-4">
              <a href={service.contactLink} className="text-blue-500 hover:underline">Contáctanos</a>
            </p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ServicesPage;
