import React from "react";
import AdCarousel from "../components/AdCarrousel";
import { useNavigate } from "react-router-dom";

const NxtHome: React.FC = () => {
const navigate = useNavigate();
 const adImages = ["./Ad1.jpeg", "./Ad2.jpeg", "./Ad3.jpeg"];
 const redirectToStore = () => {
  // Redirige a la página de la tienda (/store) al hacer clic en el botón o las imágenes
  navigate("/store");
};
  return (
    <div className="bg-white dark:bg-gray-800">
      <header className="bg-gray-900 text-white p-4 text-center">
        <h1 className="text-2xl font-bold">Nxt Activity</h1>
      </header>

      <div id="carousel" className="max-w-screen-xl mx-auto mt-4">
        <AdCarousel adImages={adImages} />
      </div>

      <div className="max-w-screen-xl mx-auto p-8">
        <div className="flex items-center justify-center">
          <img
            src="./NXTLGO.jpg"
            alt="NXT Activity Logo"
            className="rounded-full w-24 h-24"
          />
        </div>

        <div className="text-center mt-4">
          <h2 className="text-3xl font-bold text-gray-900 dark:text-white">Bienvenido a Nxt Activity</h2>
          <p className="text-gray-900 dark:text-white">
            Bienvenidos a NXT Activity, tu destino para encontrar la combinación
            perfecta entre moda y deporte.
          </p>
        </div>

        <div className="text-center mt-8">
          <h2 className="text-3xl font-bold text-gray-900 dark:text-white">Visita nuestra tienda</h2>

          <div className="flex flex-wrap justify-center mt-4">
          <img
            src="./store-image1.jpeg"
            alt="Store Image 1"
            className="w-32 h-32 object-cover m-2 rounded-lg cursor-pointer"
            onClick={redirectToStore}
          />
          <img
            src="./store-image2.jpeg"
            alt="Store Image 2"
            className="w-32 h-32 object-cover m-2 rounded-lg cursor-pointer"
            onClick={redirectToStore}
          />
          <img
            src="./store-image3.jpeg"
            alt="Store Image 3"
            className="w-32 h-32 object-cover m-2 rounded-lg cursor-pointer"
            onClick={redirectToStore}
          />
          <img
            src="./store-image4.jpeg"
            alt="Store Image 4"
            className="w-32 h-32 object-cover m-2 rounded-lg cursor-pointer"
            onClick={redirectToStore}
          />
        </div>

        <button
          className="mt-4 bg-gray-900 text-white px-6 py-3 rounded-full hover:bg-blue-700"
          onClick={redirectToStore}
        >
          Ver tienda
        </button>
        </div>
      </div>
    </div>
  );
};

export default NxtHome;
