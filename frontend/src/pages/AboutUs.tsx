import React from "react";

const AboutUsPage: React.FC = () => {
  return (
    <div className="bg-white dark:bg-gray-800">
      <div className="text-center">
        <h2 className="text-3xl font-bold mb-2 text-gray-900 dark:text-white">Conócenos</h2>
      </div>

      <div className="max-w-screen-xl mx-auto p-8 text-gray-900 dark:text-white">
        <div className="mb-8 border border-gray-900 dark:border-white p-4 rounded-lg">
          <p>
            Bienvenidos a NXT Activity, tu destino para encontrar la combinación perfecta entre moda y deporte. En NXT ACTIVITY, no solo vendemos productos, creamos experiencias. Cada artículo cuenta una historia, y cada cliente es parte de nuestra narrativa. Descubre la diferencia de elegir calidad, estilo y pasión. ¡Haz de NXT ACTIVITY tu destino para la moda activa.
          </p>
        </div>

        {/* Collage de imágenes circulares */}
        <div className="flex justify-center mb-8 space-x-4">
          <img src="./accesorios.jpg" alt="Circle Image 1" className="w-32 h-32 rounded-full object-cover" />
          <img src="./logoblanco.png" alt="Circle Image 2" className="w-32 h-32 rounded-full object-cover" />
          <img src="./NXTLGO.jpg" alt="Circle Image 3" className="w-32 h-32 rounded-full object-cover" />
          <img src="./mascota.png" alt="Circle Image 4" className="w-32 h-32 rounded-full object-cover" />
          <img src="./Tienda.jpeg" alt="Circle Image 5" className="w-32 h-32 rounded-full object-cover" />
        </div>

        <div className="flex flex-col md:flex-row mb-8 text-gray-900 dark:text-white">
          <div className="md:w-1/2 mb-4 md:mb-0 ml-32">
            <img src="./sobre-nosotros.jpeg" alt="Square Image" className="w-96 h-64 object-cover rounded-md" />
          </div>
          <div className="md:w-1/2 p-4 border border-gray-900 dark:border-white rounded-lg">
            <p>
              Nuestro deseo es mostrar a los deportes y a los eSports como iguales en el ámbito del entretenimiento y en el disfrute que puede tener respecto a cada persona. No son excluyentes y mucho menos contrarios, son disciplinas diferentes con deportistas increíbles en su respectiva área.
            </p>
          </div>
        </div>

        <div className="flex flex-col md:flex-row mb-8 text-gray-900 dark:text-white">
          <div className="md:w-1/2 order-2 md:order-1 p-4 border border-gray-900 dark:border-white rounded-lg">
            <p>
              ¿Por qué nuestro lema es "Jump in da' action"? La historia se debe a que todos en algún punto de nuestras vidas saltamos en muchos contextos diferentes. Puedes saltar de emoción por ganar una partida en tu juego favorito, puedes saltar de los nervios antes de un partido importante, puedes tomar una decisión riesgosa "de un salto" y eso puede cambiar tu vida. Todos en algún punto damos un salto, literal o figurativamente, saltamos directo a la acción. SALTA CON NOSOTROS, JUMP WITH US, JUMP IN DA' ACTION.
            </p>
          </div>
          <div className="md:w-1/2 order-1 md:order-2 mb-4 md:mb-0 ml-64">
            <img src="./equipamiento.jpeg" alt="Square Image 2" className="w-64 h-64 object-cover rounded-md" />
          </div>
        </div>

        <div className="text-center">
          <a href="/" className="text-gray-900 dark:text-white hover:underline">Regresar al inicio</a>
        </div>
      </div>
    </div>
  );
};

export default AboutUsPage;
