import React from "react";
import { useStripe, useElements, CardElement } from "@stripe/react-stripe-js";

interface PaymentFormProps {
  createOrder: (data: any, actions: any) => any;
  onApprove: (data: any, actions: any) => any;
  total_price: number;
}

const PaymentForm: React.FC<PaymentFormProps> = ({
  createOrder,
  onApprove,
  total_price,
}) => {
  const stripe = useStripe();
  const elements = useElements();

  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
  
    if (!stripe || !elements) {
      return;
    }
  
    const cardElement = elements.getElement(CardElement);
  
    if (!cardElement) {
      console.error("CardElement is missing");
      return;
    }
  
    const { token, error } = await stripe.createToken(cardElement);
  
    if (error) {
      console.error(error);
    } else {
      // Call your createOrder and onApprove functions with the token
      createOrder({ token, total_price }, {/* other data */});
      onApprove({ token }, {/* other actions */});
    }
  };
  
  return (
    <form onSubmit={handleSubmit} className="max-w-md mxl-auto p-4 border rounded shadow">
      <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
        Card details
      </label>
      <CardElement
        className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
      />
      <button
        type="submit"
        className="mt-4 px-4 py-2 text-white bg-primary-600 rounded hover:bg-primary-700 focus:outline-none focus:ring focus:border-primary-600"
        disabled={!stripe}
      >
        Pay Now
      </button>
    </form>
  );
};

export default PaymentForm;
