import React from "react";
import Slider from "react-slick";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

interface AdCarouselProps {
  adImages: string[];
}

const AdCarousel: React.FC<AdCarouselProps> = ({ adImages }) => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
  };

  return (
    <Slider {...settings}>
      {adImages.map((image, index) => (
        <div key={index}>
          <img src={image} alt={`Ad ${index + 1}`} className="w-full" />
        </div>
      ))}
    </Slider>
  );
};

export default AdCarousel;
